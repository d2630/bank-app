package digicore.bankapp.services.ServiceImpl;

import digicore.bankapp.model.Account;
import digicore.bankapp.services.AccountServices;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class AccountServiceImpl implements AccountServices {
    private static final Map<Long, Account> accountDetails = new HashMap<>();
    static class Bank{

    }
    @Override
    public Account createAccount(Account account) {
        return null;
    }

    @Override
    public Account deleteAccount(Account account) {
        return null;
    }

    @Override
    public Account updateAccount(Account account) {
        return null;
    }

    @Override
    public Account withdraw(Account account) {
        return null;
    }

    @Override
    public Account deposit(Account account) {
        return null;
    }

    @Override
    public Account accountInfo(String accountNumber) {
        return null;
    }

    @Override
    public Account accountStatement(String statement) {
        return null;
    }

    @Override
    public Account login(String accountNumber, String accountPassword) {
        return null;
    }
}
