package digicore.bankapp.services;

import digicore.bankapp.model.Account;
import digicore.bankapp.model.Account;
public interface AccountServices {
        Account createAccount(Account account);
        Account deleteAccount(Account account);
        Account updateAccount(Account account);
        Account withdraw(Account account);
        Account deposit(Account account);
        Account accountInfo(String accountNumber);
        Account accountStatement(String statement);
        Account login(String accountNumber, String accountPassword);


}
