package digicore.bankapp.repository;

import digicore.bankapp.model.Account;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class AccountRepository {
    private List<Account> list = new ArrayList<Account>();

    public void getAll() {
        List<Account> list = List.of(new Account(1, "Trinh Minh Cuong", "Developer", true, "1975-11-27"),
                new Account(2, "Mary Jane", "Banker", false, "1980-05-24"),
                new Account(3, "Tom Sawyer", "Taxi Driver", true, "1990-08-09"));
        for (Account person : list) {
            list.add(person);
        }

    }

    public List<Account> listPerson() {
        return list;
    }

    public List<Account> search(String name) {
        return list.stream().filter(x -> x.getName().startsWith(name)).collect(Collectors.toList());
    }

    public String add(Account p) {
        Account obj = new Account();
        obj.setId(p.getId());
        obj.setName(p.getName());
        obj.setJob(p.getJob());
        obj.setGender(p.isGender());
        obj.setBirthDay(p.getBirthDay());

        list.add(obj);
        return null;
    }

    public String delete(Integer id) {
        list.removeIf(x -> x.getId() == (id));
        return null;
    }

    public String edit(Account account) {
        int idx = 0;
        int id = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId() == (account.getId())) {
                id = account.getId();
                idx = i;
                break;
            }
        }

        Account p = new Account();
        p.setId(id);
        p.setName(person.getName());
        p.setJob(person.getJob());
        p.setGender(person.isGender());
        p.setBirthDay(person.getBirthDay());
        list.set(idx, p);

        return null;
    }
}
