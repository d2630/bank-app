package digicore.bankapp.controller;

import digicore.bankapp.model.Account;
import digicore.bankapp.services.AccountServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("api/")
public class AccountController {
    @Autowired
    private AccountServices accountServices;

    public AccountController(AccountServices accountServices) {
        this.accountServices = accountServices;
    }
    @PostMapping("deposit/")
    public String deposit(){
        return "";
    }
    @GetMapping("account_info/{accountNumber}")
    public String getAccountInfo(){
        return "";
    }
    @GetMapping("account_statement/{accountNumber}")
    public ResponseEntity<Account> getAccountStatement(){
        return "";
    }
    @PostMapping("withdrawal")
    public ResponseEntity<Double> withdraw(){
        return "";
    }
    @PostMapping("create_account")
    public ResponseEntity<Account> createAccount(){
        return "";
    }
    @PostMapping("login")
    public ResponseEntity<Account> login(){
        return "";
    }

}
