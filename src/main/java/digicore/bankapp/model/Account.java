package digicore.bankapp.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Account {
    private Long id;
    private String accountName;
    private String accountNumber;
    private Double balance;
    private Date transactionDate;
    private String transactionType;
    private String narration;
    private Double amount;
    private Double accountBalance;
    private Double withdrawalAmount;
    private String accountPassword;

}
